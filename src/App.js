import React, { useEffect,useState } from 'react';
import { useForm } from 'react-hook-form';
import axios from 'axios'
import MaterialDatatable from "material-datatable";

export default function App() {
  const { register, handleSubmit, errors } = useForm();
  const [data,setData] = useState([]);
  const onSubmit = data =>{
    console.log(data);

    axios.post('http://localhost:8000/persona',data)
    .then(res=>{

      console.log(res)
      cargar();

    })
  }

  const cargar = () =>{

    axios.get('http://localhost:8000/personas')
    .then(res=>{

      setData(res.data.personas);

    })
  }

  const arreglo = [

    {
      name: 'idName',
      field: 'nombre',

      options:{

            width: 94,
      },
    },

    {
      name: 'idLastname',
      field: 'apellido',

      options: {

            width: 94,
      },
    }
  ];

const info = [

  {name:"Bernardo", apellido:"Barra"},
  {name:"Rocio", apellido:"Magdalena"},

];

const opcion = {

   filterType: 'checkbox',

};

  useEffect(()=>{
    cargar();
  },[])

  useEffect(()=>{

    console.log("TODAS LAS PERSONAS:")

    console.log(data)

  },[data])

  console.log(errors);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>

      <input type="text" placeholder="nombre" name="nombre" ref={register({required: true})} />
      <input type="text" placeholder="apellido" name="apellido" ref={register} />

      <input type="submit" />

      <div>
      <MaterialDatatable
        title={"Name List"}
         info={info}
         arreglo={arreglo}
         opcion={opcion}
      />
      </div>

    </form>



  );
}
